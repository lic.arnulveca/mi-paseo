package co.com.devprofesor.mipaseo.presenter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import co.com.devprofesor.mipaseo.model.LoginInteractor;
import co.com.devprofesor.mipaseo.mvp.LoginMVP;

public class LoginPresenter implements LoginMVP.Presenter {

    private final String AUTH_PREFERENCES = "authentication";
    private final String LOGGED_KEY = "logged";

    private LoginMVP.View view;
    private LoginMVP.Model model;

    public LoginPresenter(LoginMVP.View view){
        this.view = view;
        this.model = new LoginInteractor(view.getActivity());
    }

    @Override
    public void loginWithEmail() {

        boolean error = false;

        view.showEmailError("");
        view.showPasswordError("");

        LoginMVP.LoginInfo loginInfo = view.getLoginInfo();
        if(loginInfo.getEmail().trim().isEmpty()){
            view.showEmailError("Correo Electronico Obligatorio");
            error = true;
        }else if(!isEmailValid(loginInfo.getEmail())){
            view.showEmailError("no es correo electronico");
        }

        if(loginInfo.getPassword().trim().isEmpty()){
            view.showPasswordError("contraseña es Obligatoria");
            error = true;
        }

        if(!error) {
            view.startwaiting();


            new Thread(() -> {
                model.validateCredentials(loginInfo.getEmail().trim(),
                        loginInfo.getPassword().trim(),
                        new LoginMVP.Model.ValidateCredentialsCallback() {
                            @Override
                            public void onSuccess() {


                                view.getActivity().runOnUiThread(() ->{
                                    view.stopwaiting();
                                    view.openNewActivity();
                                });
                            }

                            @Override
                            public void onFailure() {
                                view.getActivity().runOnUiThread(() ->{
                                    view.stopwaiting();
                                    view.showGeneralMessage("credenciales invalidas");
                                });
                            }
                        });
            }).start();
        }

    }

    private boolean isEmailValid(String email){
        return email.contains("@") && email.endsWith(".com");
    }

    @Override
    public void loginWithFacebook() {

    }

    @Override
    public void loginWithGoogle() {
        Intent intent = model.getGoogleIntent();
        view.openGoogleSingInActivity(intent);
    }

    @Override
    public void rememberPassword() {

    }

    @Override
    public void validationEmail() {

    }

    @Override
    public void isAuthenticated() {
        boolean isAuthenticated = model.isAuthenticated();
        if(isAuthenticated){
            view.openNewActivity();
        }
    }

    @Override
    public void setGoogleData(Intent data) {
        view.startwaiting();
        model.setGoogleData(data, new LoginMVP.Model.ValidateCredentialsCallback() {
            @Override
            public void onSuccess() {
                view.getActivity().runOnUiThread(() ->{
                    view.stopwaiting();
                    view.openNewActivity();
                });
            }

            @Override
            public void onFailure() {
                    view.getActivity().runOnUiThread(() ->{
                    view.stopwaiting();
                    view.showGeneralMessage("no pudo acceder a su cuenta de gmail");

        });
        }
        });

}
    }




