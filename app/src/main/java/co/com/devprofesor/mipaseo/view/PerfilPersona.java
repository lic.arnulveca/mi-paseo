package co.com.devprofesor.mipaseo.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import co.com.devprofesor.mipaseo.R;
import co.com.devprofesor.mipaseo.view.adapter.InscribirUnSitio;

public class PerfilPersona extends AppCompatActivity {

    private Button btn_places_visitades;
    private Button btn_inscribirunsitio;
    private Button btn_compras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_persona);

        initPanta();
    }

    private void initPanta() {
        //inicio lugares visitados
        btn_places_visitades = findViewById(R.id.btn_places_visitades);
        btn_places_visitades.setOnClickListener((evt) -> {
            onPlacesVisitaded();
        });
        //inicio inscribir un sitio
        btn_inscribirunsitio = findViewById(R.id.btn_inscribirunsitio);
        btn_inscribirunsitio.setOnClickListener((evt) -> {
            OnInscribirUnSitio();
        });

        //inicio inscribir un sitio
        btn_compras = findViewById(R.id.btn_compras);
        btn_compras.setOnClickListener((evt) -> {
            OnCompras();
        });
    }

    private void OnCompras() {
        Toast.makeText(this, "vamos a compras", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, cambiarPuntosxProductos.class);
        startActivity(intent);
    }

    private void OnInscribirUnSitio() {
        Toast.makeText(this, "Vamos a inscribir un sitio", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, InscribirUnSitio.class);
        startActivity(intent);
    }


    private void onPlacesVisitaded() {
        Toast.makeText(this, "Mis sitios visitados abierto", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, PlacesVisited.class);
        startActivity(intent);
    }

}