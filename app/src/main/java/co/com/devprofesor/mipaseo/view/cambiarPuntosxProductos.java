package co.com.devprofesor.mipaseo.view;


import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.leanback.widget.Presenter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import java.util.List;

import co.com.devprofesor.mipaseo.R;
import co.com.devprofesor.mipaseo.mvp.CambiarpuntosxproductosMVP;
import co.com.devprofesor.mipaseo.presenter.CambiarpuntosxproductosPresenter;
import co.com.devprofesor.mipaseo.view.adapter.productosAdapter;

public class cambiarPuntosxProductos extends AppCompatActivity implements CambiarpuntosxproductosMVP.View {

    private DrawerLayout drawerLayout;
    private MaterialToolbar topAppBar;
    private NavigationView navegaciondibujo;

    private LinearProgressIndicator progres_carga_documento;

    private RecyclerView rvVentaspopuntos;
    private FloatingActionButton floatingActionButton;

    private CambiarpuntosxproductosMVP.Presenter presenter;

    private productosAdapter productosAdapter;

     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambiar_puntosx_productos);

        presenter = new CambiarpuntosxproductosPresenter(this);

        initUI();

        presenter.loadProductos();

    }

    private void initUI() {
        drawerLayout = findViewById(R.id.drawerLayout);
        topAppBar = findViewById(R.id.topAppBar);
        navegaciondibujo = findViewById(R.id.navegaciondibujo);

        progres_carga_documento = findViewById(R.id.progres_carga_documento);

        topAppBar.setNavigationOnClickListener(v -> openDrawer());
        navegaciondibujo.setNavigationItemSelectedListener(this::NavigationSelection);
        rvVentaspopuntos = findViewById(R.id.vtventasporpuntos);

        rvVentaspopuntos.setLayoutManager(new GridLayoutManager(cambiarPuntosxProductos.this, 2));
        productosAdapter = new productosAdapter();
        rvVentaspopuntos.setAdapter(productosAdapter);

        floatingActionButton = findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(v -> presenter.showNewVenta());
        }


    private void openDrawer(){
        drawerLayout.openDrawer(navegaciondibujo);
    }

    private boolean NavigationSelection(MenuItem menuItem){
        menuItem.setChecked(true);
        Toast.makeText(this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        drawerLayout.closeDrawer(navegaciondibujo);
        return true;
    }

    @Override
    public Activity getActivity() {
        return cambiarPuntosxProductos.this;
    }

    @Override
    public void showNewVentaBoton() {
        startActivity(new Intent(cambiarPuntosxProductos.this, VentaCuadro.class));
    }

    @Override
    public void showproductosInfo(List<CambiarpuntosxproductosMVP.ProductosInfo> productosInfo) {
        //cargar la información en el recycler view
        productosAdapter.setData(productosInfo);
        Toast.makeText(cambiarPuntosxProductos.this, "datos cargados", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void shoeProgressBar() {
        progres_carga_documento.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progres_carga_documento.setVisibility(View.GONE);
    }
}


