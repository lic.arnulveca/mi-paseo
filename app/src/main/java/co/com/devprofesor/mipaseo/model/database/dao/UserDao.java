package co.com.devprofesor.mipaseo.model.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import co.com.devprofesor.mipaseo.model.database.entities.User;

@Dao
public interface UserDao {

    //TODOS LOS USUARIOS
    @Query("SELECT * FROM user")
    List<User> getAll();

    //UN USUARIO POR CORREO ELECTRONICO
    @Query("SELECT * FROM user WHERE email = :email")
    User getUserByEmail(String email);

    //insertar usuarios
    @Insert
    void insertAll(User... users);

    //borrar campos
    @Delete
    void delete(User user);

}
