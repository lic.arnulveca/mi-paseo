package co.com.devprofesor.mipaseo.model.database;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import co.com.devprofesor.mipaseo.model.database.dao.UserDao;
import co.com.devprofesor.mipaseo.model.database.entities.Puntos;
import co.com.devprofesor.mipaseo.model.database.entities.User;

@Database(entities = {User.class, Puntos.class}, version = 1)

public abstract class DataBaseApp extends RoomDatabase {


    public abstract UserDao getUserDao();

    private static volatile DataBaseApp INSTANCE;

    public static DataBaseApp getDatabase(Context context) {

        if (INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), DataBaseApp.class, "DataBase-name")
                    .allowMainThreadQueries().build();

        }return INSTANCE;
    }

}
