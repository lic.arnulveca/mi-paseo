package co.com.devprofesor.mipaseo.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import co.com.devprofesor.mipaseo.R;
import co.com.devprofesor.mipaseo.model.CambiarpuntosxproductosInteractor;
import co.com.devprofesor.mipaseo.mvp.CambiarpuntosxproductosMVP;

public class productosAdapter extends RecyclerView.Adapter<productosAdapter.ViewHolder> {

    private List<CambiarpuntosxproductosMVP.ProductosInfo> data;


    public productosAdapter() {
        this.data = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.venta_productos_lista, parent, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        CambiarpuntosxproductosMVP.ProductosInfo item = data.get(position);

        holder.getTvproducto().setText(item.getName());
        //holder.getIvproducto().setImageIcon();
        holder.getTvCosto().setText(item.getCosto());

        holder.itemView.setOnClickListener(v ->{
            Toast.makeText(v.getContext(), item.getName(), Toast.LENGTH_SHORT).show();

        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<CambiarpuntosxproductosMVP.ProductosInfo> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView ivproducto;
        private TextView tvproducto;
        private TextView tvCosto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivproducto = itemView.findViewById(R.id.productos_numeral);
            tvproducto = itemView.findViewById(R.id.nombre_delproducto);
            tvCosto = itemView.findViewById(R.id.costo_producto);
            /* aqui puedo hacer evento al darle clic a un elemento de la vista
            itemView.setOnClickListener(v ->{
                Toast.makeText(itemView.getContext(), tvproducto.getText().toString(), Toast.LENGTH_SHORT).show();

            });*/

        }

        public ImageView getIvproducto() {
            return ivproducto;
        }

        public TextView getTvproducto() {
            return tvproducto;
        }

        public TextView getTvCosto() {
            return tvCosto;
        }
    }

}
