package co.com.devprofesor.mipaseo.presenter;

import android.os.Bundle;

import java.util.List;

import co.com.devprofesor.mipaseo.model.CambiarpuntosxproductosInteractor;
import co.com.devprofesor.mipaseo.model.repository.placesVisitadesInteractor;
import co.com.devprofesor.mipaseo.mvp.CambiarpuntosxproductosMVP;
import co.com.devprofesor.mipaseo.mvp.PlacesVisitadesMvp;

public class PlacesVisitadedPresenter implements PlacesVisitadesMvp.Presenter {

    private PlacesVisitadesMvp.View view;
    private PlacesVisitadesMvp.Model model;


    public PlacesVisitadedPresenter(PlacesVisitadesMvp.View view){
        this.view = view;
        this.model = new placesVisitadesInteractor();
    }

    @Override
    public void loadProductos() {

        view.shoeProgressBar();

        new Thread(() -> {
            model.loadProductos(new PlacesVisitadesMvp.Model.LoadProductosCallback() {
                @Override
                public void showProductosInfo(List<PlacesVisitadesMvp.ProductosInfo> productosInfo) {

                    view.getActivity().runOnUiThread(() -> {
                        view.showproductosInfo(productosInfo);
                        view.hideProgressBar();
                    });
                }
            });
        }).start();
    }

    @Override
    public void onItemSelected(PlacesVisitadesMvp.ProductosInfo info) {
        Bundle params = new Bundle();
        params.putString("nombredelsitio", info.getNombredelsitio());
        params.putString("descripciondelsitio", info.getDescripciondelsitio());
        params.putString("ubicacion", info.getUbicacion());

        view.openLocationActivivity(params);
    }


}
