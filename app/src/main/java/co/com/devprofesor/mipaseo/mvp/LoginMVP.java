package co.com.devprofesor.mipaseo.mvp;

import android.app.Activity;
import android.content.Intent;

public interface LoginMVP {

    interface Model{

        void validateCredentials(String email, String password, ValidateCredentialsCallback callback);

        boolean isAuthenticated();

        Intent getGoogleIntent();

        void setGoogleData(Intent data, ValidateCredentialsCallback callback);


        interface ValidateCredentialsCallback{
            void onSuccess();
            void onFailure();
        }
    }

    interface Presenter{
        void loginWithEmail();
        void loginWithFacebook();
        void loginWithGoogle();
        void rememberPassword();
        void validationEmail();


        void isAuthenticated();

        void setGoogleData(Intent data);
    }




    interface View{

        Activity getActivity();

        LoginInfo getLoginInfo();
        void showEmailError(String error);
        void showPasswordError(String error);
        void showGeneralMessage(String error);

        void clearData();

        void openNewActivity();

        void startwaiting();

        void stopwaiting();

        void openGoogleSingInActivity(Intent intent);
    }

    class LoginInfo{
        private String email;
        private String password;

        public LoginInfo(String email, String password){
            this.email = email;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }


}
