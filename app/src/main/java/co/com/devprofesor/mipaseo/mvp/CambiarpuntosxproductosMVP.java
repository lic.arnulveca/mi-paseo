package co.com.devprofesor.mipaseo.mvp;

import android.app.Activity;

import java.util.List;

public interface CambiarpuntosxproductosMVP {

    interface Model{
        void loadProductos(LoadProductosCallback callback);

        interface LoadProductosCallback{
            void showProductosInfo(List<ProductosInfo> productosInfo);
        }

    }

    interface Presenter{
        void loadProductos();
        void showNewVenta();
    }

    interface View{

        Activity getActivity();

        void showNewVentaBoton();

        void showproductosInfo(List<ProductosInfo> productosInfo);

        void shoeProgressBar();

        void hideProgressBar();


    }

    class ProductosInfo{
        private String imagename;
        private String name;
        private String costo;

        public ProductosInfo(String name, String costo) {
            this(null, name, costo);
        }

        public ProductosInfo(String imagename, String name, String costo) {
            this.imagename = imagename;
            this.name = name;
            this.costo = costo;
        }

        public String getImagename() {
            return imagename;
        }

        public String getName() {
            return name;
        }

        public String getCosto() {
            return costo;
        }
    }

}
