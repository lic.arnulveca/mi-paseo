package co.com.devprofesor.mipaseo.presenter;

import java.util.List;

import co.com.devprofesor.mipaseo.model.CambiarpuntosxproductosInteractor;
import co.com.devprofesor.mipaseo.mvp.CambiarpuntosxproductosMVP;

public class CambiarpuntosxproductosPresenter  implements CambiarpuntosxproductosMVP.Presenter{

    private CambiarpuntosxproductosMVP.View view;
    private CambiarpuntosxproductosMVP.Model model;


    public CambiarpuntosxproductosPresenter(CambiarpuntosxproductosMVP.View view){
        this.view = view;
        this.model = new CambiarpuntosxproductosInteractor();
    }

    @Override
    public void loadProductos() {

        view.shoeProgressBar();

        new Thread(() -> {
          model.loadProductos(new CambiarpuntosxproductosMVP.Model.LoadProductosCallback() {
              @Override
              public void showProductosInfo(List<CambiarpuntosxproductosMVP.ProductosInfo> productosInfo) {

                  view.getActivity().runOnUiThread(() -> {
                      view.showproductosInfo(productosInfo);
                      view.hideProgressBar();
                  });
              }
          });
        }).start();
    }

    @Override
    public void showNewVenta() {
        view.showNewVentaBoton();
    }
}
