package co.com.devprofesor.mipaseo.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.progressindicator.LinearProgressIndicator;

import java.util.List;

import co.com.devprofesor.mipaseo.R;
import co.com.devprofesor.mipaseo.mvp.CambiarpuntosxproductosMVP;
import co.com.devprofesor.mipaseo.mvp.PlacesVisitadesMvp;
import co.com.devprofesor.mipaseo.presenter.CambiarpuntosxproductosPresenter;
import co.com.devprofesor.mipaseo.presenter.PlacesVisitadedPresenter;
import co.com.devprofesor.mipaseo.view.adapter.productosAdapter;
import co.com.devprofesor.mipaseo.view.adapter.productosAdapterPlaces;

public class PlacesVisited extends AppCompatActivity implements PlacesVisitadesMvp.View {


    private LinearProgressIndicator progres_carga_documento;
    private RecyclerView vtSitiosvisitados;
    private PlacesVisitadesMvp.Presenter presenter;
    private productosAdapterPlaces productosAdapterPlaces;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places_visitad);

        presenter = new PlacesVisitadedPresenter(this);

        initUI();

        presenter.loadProductos();
    }
    private void initUI() {

        progres_carga_documento = findViewById(R.id.progres_carga_documento);
        vtSitiosvisitados = findViewById(R.id.vtSitiosvisitados);

        vtSitiosvisitados.setLayoutManager(new LinearLayoutManager(PlacesVisited.this));
        productosAdapterPlaces = new productosAdapterPlaces();
        vtSitiosvisitados.setAdapter(productosAdapterPlaces);

        productosAdapterPlaces.setOnItemClickListener(info -> presenter.onItemSelected(info));

    }
    /////////////77

    @Override
    public Activity getActivity() {
        return PlacesVisited.this;
    }


    @Override
    public void showproductosInfo(List<PlacesVisitadesMvp.ProductosInfo> productosInfo) {
        productosAdapterPlaces.setData(productosInfo);
        Toast.makeText(PlacesVisited.this, "datos cargados", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void shoeProgressBar() {
        progres_carga_documento.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progres_carga_documento.setVisibility(View.GONE);
    }

    @Override
    public void openLocationActivivity(Bundle params) {
        Intent intent = new Intent(PlacesVisited.this, LocationActivity.class);
        intent.putExtras(params);
        startActivity(intent);


    }
}