package co.com.devprofesor.mipaseo.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import co.com.devprofesor.mipaseo.mvp.CambiarpuntosxproductosMVP;

public class CambiarpuntosxproductosInteractor implements CambiarpuntosxproductosMVP.Model {

    private List<CambiarpuntosxproductosMVP.ProductosInfo> data;

    public CambiarpuntosxproductosInteractor(){
        data = Arrays.asList(
              new CambiarpuntosxproductosMVP.ProductosInfo("Gorra Viajera", "$10000"),
              new CambiarpuntosxproductosMVP.ProductosInfo("pantalon Viajera", "$60000"),
              new CambiarpuntosxproductosMVP.ProductosInfo("Camisa Viajera", "$30000"),
              new CambiarpuntosxproductosMVP.ProductosInfo("pantaloneta Viajera", "$20000"),
              new CambiarpuntosxproductosMVP.ProductosInfo("Chaqueta viajera", "$50000")
        );
    }

    @Override
    public void loadProductos(LoadProductosCallback callback) {

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        callback.showProductosInfo(this.data);
    }
}
