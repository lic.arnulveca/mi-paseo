package co.com.devprofesor.mipaseo.model.database.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Puntos {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private int puntos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }


}
