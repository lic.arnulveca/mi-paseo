package co.com.devprofesor.mipaseo.view;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.SignInButton;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import co.com.devprofesor.mipaseo.R;
import co.com.devprofesor.mipaseo.mvp.LoginMVP;
import co.com.devprofesor.mipaseo.presenter.LoginPresenter;

public class LoginActivity extends AppCompatActivity implements LoginMVP.View {
/* aqui agrego cada una de las variables del login */
    private final static String EMAIL_KEY = "email";
    private final static String PASSWORD_KEY = "password";

    private TextInputEditText correoElectronico;
    private TextInputEditText Password;
    private TextInputLayout titulocorreoElectronico;
    private TextInputLayout Passwordtitulo;
    private LinearProgressIndicator pi_waiting;

    private Button btnIngresar;
    private Button btnRegistrarme;
    private AppCompatButton btnFacebook;
    private SignInButton btnGoogle;

    private ActivityResultLauncher<Intent> googleLauncher;

    LoginMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /* con el codigo de abajo se inicia la pantalla */

        presenter = new LoginPresenter(this);
        presenter.isAuthenticated();
        initPanta();

    }



    private void initPanta() {

        pi_waiting = findViewById(R.id.pi_waiting);

        correoElectronico = findViewById(R.id.correoElectronico);
        Password = findViewById(R.id.Password);
        titulocorreoElectronico = findViewById(R.id.titulocorreoElectronico);
        Passwordtitulo = findViewById(R.id.Passwordtitulo);

        btnRegistrarme = findViewById(R.id.btn_Registrarme);
        btnIngresar = findViewById(R.id.btn_Ingresar);
        btnFacebook = findViewById(R.id.btn_Facebook);
        btnGoogle = findViewById(R.id.btn_Google);


        /*de esta forma se activan los botones las respuestas sonb abajo e independientes*/

        btnRegistrarme.setOnClickListener((evt) -> {
            onRegistrarmeClick();
        });

        btnFacebook.setOnClickListener((evt) -> presenter.loginWithFacebook());

        btnGoogle.setOnClickListener((evt) -> presenter.loginWithGoogle());

        btnIngresar.setOnClickListener((evt) -> presenter.loginWithEmail());

        // inicio de google
        googleLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(), result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        presenter.setGoogleData(data);
                    }else {
                        stopwaiting();
                    }
                });

    }
    /* respuestas de los botones
    private void onIngresarClick(){
        correoElectronico.setText("");
        Password.setText("");
        Toast.makeText(this, "inicio de sesion correcto", Toast.LENGTH_SHORT).show();
    }
*/
    private void onRegistrarmeClick() {
        Toast.makeText(this, "pagina de registro abierta", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, Registrarse.class);
        startActivity(intent);
    }
    /*
        private void onFacebookClick() {
            Toast.makeText(this, "inicio de sesion con facebook", Toast.LENGTH_SHORT).show();
        }

        private void onGoogleClick() {
            Toast.makeText(this, "inicio de sesion con google", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, cambiarPuntosxProductos.class);
            startActivity(intent);
        }
    */
    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(correoElectronico.getText().toString(), Password.getText().toString());
    }

    @Override
    public void showEmailError(String error) {
        titulocorreoElectronico.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        Passwordtitulo.setError(error);
    }

    @Override
    public void showGeneralMessage(String error) {
        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearData() {
        correoElectronico.setError("");
        titulocorreoElectronico.setError("");
        Password.setError("");
        Passwordtitulo.setError("");
    }

    @Override
    public void openNewActivity() {
        Intent intent = new Intent(this, LocationActivity.class);

        startActivity(intent);
    }

    @Override
    public void startwaiting() {
        pi_waiting.setVisibility(View.VISIBLE);
        btnIngresar.setEnabled(false);
        btnFacebook.setEnabled(false);
        btnGoogle.setEnabled(false);
        btnRegistrarme.setEnabled(false);
    }

    @Override
    public void stopwaiting() {
        pi_waiting.setVisibility(View.INVISIBLE);
        btnIngresar.setEnabled(true);
        btnFacebook.setEnabled(true);
        btnGoogle.setEnabled(true);
        btnRegistrarme.setEnabled(true);
    }

    @Override
    public void openGoogleSingInActivity(Intent intent) {


        googleLauncher.launch(intent);
        //startActivityForResult(intent, 1);
    }


    public Activity getActivity(){
        return this;
    }

}
