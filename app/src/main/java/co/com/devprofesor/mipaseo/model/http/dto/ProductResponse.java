package co.com.devprofesor.mipaseo.model.http.dto;

  /*
    {
        "_id": "61644e2f335aa93ff030d197",
            "nombre": "Jeniffer",
            "apellido": "Estrada",
            "edad": 18,
            "email": "jenian19@gmail.com",
            "__v": 0
    }

    */

public class ProductResponse {

    private String nombre;
    private String Apellido;
    private Integer edad;
    private String email;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String _id;

    public void set_id(String _id) {
        this._id = _id;
    }

    public void set__v(String __v) {
        this.__v = __v;
    }

    private String __v;

    @Override
    public String toString() {
        return "ProductResponse{" +
                "nombre='" + nombre + '\'' +
                ", Apellido='" + Apellido + '\'' +
                ", edad=" + edad +
                ", email='" + email + '\'' +
                '}';
    }
}
