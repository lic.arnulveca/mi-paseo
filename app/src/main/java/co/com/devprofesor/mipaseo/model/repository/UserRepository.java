package co.com.devprofesor.mipaseo.model.repository;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Delete;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import co.com.devprofesor.mipaseo.model.database.DataBaseApp;
import co.com.devprofesor.mipaseo.model.database.dao.UserDao;
import co.com.devprofesor.mipaseo.model.database.entities.User;

public class UserRepository {

    private final static Boolean USE_DATABASE = Boolean.FALSE;

    private UserDao userDao;

    private DatabaseReference userRef;

    private static UserRepository instance;

    public static UserRepository getInstance(Context context) {
        if(instance == null){
            instance = new UserRepository(context);
        }
        return instance;
    }

    public UserRepository(Context context) {

        userDao = DataBaseApp.getDatabase(context).getUserDao();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        userRef = database.getReference("users");

        loadInitialDatabase();
    }

    private void loadInitialDatabase() {

        if(USE_DATABASE) {
            userDao.insertAll(
                    new User("Arnulfo Vega", "lic.arnulveca@gmail.com", "12345678"),
                    new User("GATOI", "prueba@gmmail.com", "12345678")
            );
        }else {

            String username = "Cesardiaz@gmail.com".replace("@", "_").replace(".", "_");

            userRef.child(username).child("name").setValue("Cesar diaz");
            userRef.child(username).child("email").setValue("Cesardiaz@gmail.com");
            userRef.child(username).child("password").setValue("1234");

            username = "prueba@gmmail.com".replace("@", "_").replace(".", "_");
            userRef.child(username)
                    .setValue(new User("GATOI", "prueba@gmmail.com", "12345678"));

            username = "lic.arnulveca@gmail.com".replace("@", "_").replace(".", "_");
            userRef.child(username)
                    .setValue(new User("Arnulfo Vega", "lic.arnulveca@gmail.com", "12345678"));
        }
    }

    public void getUserByEmail(String email, UserCallback<User> callback) {
        if (USE_DATABASE){
            callback.onSuccess(userDao.getUserByEmail(email));
        }else{
            //usar firebase
            // Read from the database
            String username = email.replace("@", "_").replace(".", "_");
            userRef.child(username).get().addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    User value = task.getResult().getValue(User.class);
                    if (value != null) {
                        Log.i(UserRepository.class.getSimpleName(), value.toString());
                    }
                    callback.onSuccess(value);
                }else {
                    callback.onFailure();
                }
            });
        }
    }

    public void getAll(UserCallback<List<User>> callback){
        userRef.get().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                DataSnapshot dataSnapshot = task.getResult();
                if(dataSnapshot.hasChildren()){
                    List<User> users = new ArrayList<>();

                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                        User user = snapshot.getValue(User.class);
                        Log.d(UserRepository.class.getSimpleName(), user.toString());
                        users.add(user);

                    }
                    callback.onSuccess(users);
                }else {
                    callback.onSuccess(new ArrayList<>());
                }
            }else{
                callback.onFailure();
            }
        });
    }

    private User getUserByEmailDB(String email){
        return userDao.getUserByEmail(email);
    }

    public interface UserCallback<T>{
        void onSuccess(T data);
        void onFailure();
    }




}
