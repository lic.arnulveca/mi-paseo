package co.com.devprofesor.mipaseo.mvp;

import android.app.Activity;
import android.os.Bundle;

import java.util.List;

public interface PlacesVisitadesMvp {

    interface Model{
        void loadProductos(LoadProductosCallback callback);

        interface LoadProductosCallback{
            void showProductosInfo(List<ProductosInfo> productosInfo);
        }

    }

    interface Presenter{
        void loadProductos();

        void onItemSelected(ProductosInfo info);
    }

    interface View{

        Activity getActivity();


        void showproductosInfo(List<ProductosInfo> productosInfo);

        void shoeProgressBar();

        void hideProgressBar();


        void openLocationActivivity(Bundle params);
    }

    class ProductosInfo{

        private String Nombredelsitio;
        private String Descripciondelsitio;
        private String ubicacion;

        public ProductosInfo(String nombredelsitio, String descripciondelsitio, String ubicacion) {
            this.Nombredelsitio = nombredelsitio;
            this.Descripciondelsitio = descripciondelsitio;
            this.ubicacion = ubicacion;
        }


        public String getNombredelsitio() {
            return Nombredelsitio;
        }

        public String getDescripciondelsitio() {
            return Descripciondelsitio;
        }

        public String getUbicacion() {
            return ubicacion;
        }

    }

}
