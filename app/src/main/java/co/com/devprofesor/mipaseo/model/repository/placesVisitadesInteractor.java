package co.com.devprofesor.mipaseo.model.repository;

import java.util.Arrays;
import java.util.List;


import co.com.devprofesor.mipaseo.mvp.PlacesVisitadesMvp;

public class placesVisitadesInteractor implements PlacesVisitadesMvp.Model {

    private List<PlacesVisitadesMvp.ProductosInfo> data;

    public placesVisitadesInteractor(){
        data = Arrays.asList(
                new PlacesVisitadesMvp.ProductosInfo("parque central del norte", "es un bello sitio con gran arquitectura", "Barrio boyaca Duitama Boyaca"),
                new PlacesVisitadesMvp.ProductosInfo("Catedral de duitama", "es un bello sitio con gran arquitectura", "Catedral de Duitama Boyaca"),
                new PlacesVisitadesMvp.ProductosInfo("la trinidad", "es una vereda de la ciudad", "la trinidad Duitama Boyaca"),
                new PlacesVisitadesMvp.ProductosInfo("Represa del sisga", "que linda laguna", "represa del sisga cundinamarca"),
                new PlacesVisitadesMvp.ProductosInfo("Laguna del cacique guatavita", "excelente sitio para visitar", "Laguna del cacique guatavita")
        );
    }

    @Override
    public void loadProductos(PlacesVisitadesMvp.Model.LoadProductosCallback callback) {

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        callback.showProductosInfo(this.data);
    }
}
