package co.com.devprofesor.mipaseo.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import co.com.devprofesor.mipaseo.R;

public class Registrarse extends AppCompatActivity {

    private Button btn_register;
    private Button btn_gotoinit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarse);

        initPanta();
    }

    private void initPanta() {

        btn_gotoinit = findViewById(R.id.btn_gotoinit);
        btn_gotoinit.setOnClickListener((evt) -> { onInitpant(); });

    }

    private void onInitpant() {
        Toast.makeText(this, "vamos a la pantalla de inicio", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}