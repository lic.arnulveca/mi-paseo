package co.com.devprofesor.mipaseo.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import co.com.devprofesor.mipaseo.R;
import co.com.devprofesor.mipaseo.mvp.CambiarpuntosxproductosMVP;
import co.com.devprofesor.mipaseo.mvp.PlacesVisitadesMvp;

public class productosAdapterPlaces extends RecyclerView.Adapter<productosAdapterPlaces.ViewHolder> {

    private List<PlacesVisitadesMvp.ProductosInfo> data;
    private ViewHolder.OnItemClickListener onItemClickListener;

    public productosAdapterPlaces() {
        this.data = new ArrayList<>();
    }

    public void setOnItemClickListener(ViewHolder.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.places_visited, parent, false);
                return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull productosAdapterPlaces.ViewHolder holder, int position) {
        PlacesVisitadesMvp.ProductosInfo item = data.get(position);

        holder.getNombre_del_sitio().setText(item.getNombredelsitio());
        //holder.getIvproducto().setImageIcon();
        holder.getDescripciondelsitio().setText(item.getDescripciondelsitio());
        holder.getUbicacion().setText(item.getUbicacion());

        if(onItemClickListener != null){
            holder.itemView.setOnClickListener(v ->{
                onItemClickListener.onItemClick(item);

            });
        }


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<PlacesVisitadesMvp.ProductosInfo> data) {
        this.data = data;
        notifyDataSetChanged();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView nombre_del_sitio;
        private TextView descripciondelsitio;
        private TextView ubicacion;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nombre_del_sitio = itemView.findViewById(R.id.nombre_del_sitio);
            descripciondelsitio = itemView.findViewById(R.id.descripciondelsitio);
            ubicacion = itemView.findViewById(R.id.ubicacion);
            /* aqui puedo hacer evento al darle clic a un elemento de la vista
            itemView.setOnClickListener(v ->{
                Toast.makeText(itemView.getContext(), tvproducto.getText().toString(), Toast.LENGTH_SHORT).show();

            });*/

        }
        public TextView getNombre_del_sitio() {
            return nombre_del_sitio;
        }

        public TextView getDescripciondelsitio() {
            return descripciondelsitio;
        }

        public TextView getUbicacion() {
            return ubicacion;
        }

        public interface OnItemClickListener{
            void onItemClick(PlacesVisitadesMvp.ProductosInfo info);
        }

    }
}
