package co.com.devprofesor.mipaseo.view;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseUser;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


import co.com.devprofesor.mipaseo.R;
import co.com.devprofesor.mipaseo.model.repository.FirebaseAuthRepository;


public class LocationActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    SearchView searchView;
    SupportMapFragment mapFragment;
    private Button btn_home;
    private Button btn_user;
    private Button btn_sale;
    private Button btn_points;
    public TextView usertitle;

    private FusedLocationProviderClient fusedLocationProviderClient;
    private Location location;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        searchView = findViewById(R.id.lineadebusqueda);
        initPanta();
        loadData();
        requestPermissions();
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestLocation();

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);


        String sitio = getIntent().getStringExtra("nombredelsitio");
        String ubicacion = getIntent().getStringExtra("ubicacion");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

        // Asigno un nivel de zoom
        CameraUpdate ZoomCam = CameraUpdateFactory.zoomTo(14f);
        mMap.animateCamera(ZoomCam);

        // Establezco un listener para ver cuando cambio de posicion
        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

            public void onMyLocationChange(Location pos) {
                // TODO Auto-generated method stub

                // Extraigo la Lat y Lon del Listener
                double lat = pos.getLatitude();
                double lon = pos.getLongitude();

                // Muevo la camara a mi posicion
                CameraUpdate cam = CameraUpdateFactory.newLatLng(new LatLng(
                        lat, lon));

                mMap.moveCamera(cam);

            }
        });
        // Add a marker in Sydney and move the camera

        try {
            Geocoder geo = new Geocoder(LocationActivity.this.getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocationName(ubicacion, 1);
            if (!addresses.isEmpty()) {
                LatLng location = new LatLng(4, -72);
                location = new LatLng(addresses.get(0).getLatitude(),
                        addresses.get(0).getLongitude());
                mMap.addMarker(new MarkerOptions()
                        .position(location)
                        .title(sitio + " - " + ubicacion));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 14f));
            }
        } catch (Exception e) {
            e.printStackTrace(); // getFromLocation() may sometimes fail
        }

        // Add a marker in Sydney and move the camera

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String location = searchView.getQuery().toString();
                List<Address> addressList = null;
                if(location != null || !location.equals("")){
                    Geocoder geocoder = new Geocoder(LocationActivity.this);
                    try {
                        addressList=geocoder.getFromLocationName(location, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Address address = addressList.get(0);
                    LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(latLng).title(location));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f  ));
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }



    @RequiresApi(api = Build.VERSION_CODES.N)
    private void requestPermissions() {
        // Obtener permisos para el GPS
        ActivityResultLauncher<String[]> locationPermissionRequest =
                registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(),
                        result -> {
                            Boolean fineLocationGranted = result.getOrDefault(
                                    Manifest.permission.ACCESS_FINE_LOCATION, false);
                            Boolean coarseLocationGranted = result.getOrDefault(
                                    Manifest.permission.ACCESS_COARSE_LOCATION, false);
                            if (fineLocationGranted != null && fineLocationGranted) {
                                // Precise location access granted.
                            } else if (coarseLocationGranted != null && coarseLocationGranted) {
                                // Only approximate location access granted.
                            } else {
                                // No location access granted.
                            }
                        }
                );

        locationPermissionRequest.launch(new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        });

    }



    private void requestLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                requestPermissions();
            }
            return;
        }
        fusedLocationProviderClient.getLastLocation()
                .addOnFailureListener(e -> {
                    Log.e("LocationActivity", "No pudo obtener la posición", e);
                })
                .addOnSuccessListener(this,
                        location -> {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                // Logic to handle location object
                                this.location = location;

                                Toast.makeText(LocationActivity.this,
                                        this.location.getLatitude() + "," + this.location.getLongitude(),
                                        Toast.LENGTH_SHORT).show();


                            } else {
                                Toast.makeText(LocationActivity.this, "No hay ubicación actualmente",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
    }



    private void initPanta() {
        btn_home = findViewById(R.id.btn_home);
        btn_user = findViewById(R.id.btn_user);
        btn_sale = findViewById(R.id.btn_sale);
        btn_points = findViewById(R.id.btn_points);
        usertitle = findViewById(R.id.usertitle);

        btn_home.setOnClickListener((evt) -> { onHomeclick(); });
        btn_user.setOnClickListener((evt) -> { onUserclick(); });
        btn_sale.setOnClickListener((evt) -> { onSaleclick(); });
        btn_points.setOnClickListener((evt) -> { onPointsclick(); });


        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);


    }

    private void onPointsclick() {
        Toast.makeText(this, "intentas cerrar sesion", Toast.LENGTH_SHORT).show();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);


        builder.setTitle(R.string.app_name)
                .setMessage("Esta seguro de cerrar sesion")
                .setPositiveButton("OK", (dialog, which) -> {
                    FirebaseAuthRepository.getInstance(LocationActivity.this).logout(new FirebaseAuthRepository.FirebaseAuthCallBack() {
                        @Override
                        public void onSuccess() {

                            Intent intent = new Intent(LocationActivity.this, LoginActivity.class);
                            startActivity(intent);

                        }

                        @Override
                        public void onFailure() {
                            Toast.makeText(LocationActivity.this, "error al cerrar sesion", Toast.LENGTH_SHORT).show();
                        }
                    });

                })
                .setNegativeButton("CANCELAR", null);

        builder.create().show();
    }



    private void onSaleclick() {
        Toast.makeText(this, "vamos a compras y ventas", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, cambiarPuntosxProductos.class);
        startActivity(intent);
    }

    private void onUserclick() {
        Toast.makeText(this, "perfil de la persona abierto", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, PerfilPersona.class);
        startActivity(intent);
    }

    private void onHomeclick() {
        Toast.makeText(this, "pagina de home", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, LocationActivity.class);
        startActivity(intent);
    }

    private void loadData(){


        FirebaseAuthRepository repository = FirebaseAuthRepository.getInstance(this);
        FirebaseUser user = repository.getCurrentUser();
        usertitle.setText(user.getDisplayName());
    }


}