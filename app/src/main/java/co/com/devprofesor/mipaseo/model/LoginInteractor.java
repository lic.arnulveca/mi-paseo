package co.com.devprofesor.mipaseo.model;

import android.content.Context;
import android.content.Intent;

import co.com.devprofesor.mipaseo.model.repository.FirebaseAuthRepository;
import co.com.devprofesor.mipaseo.model.repository.ProductRepository;
import co.com.devprofesor.mipaseo.model.repository.UserRepository;
import co.com.devprofesor.mipaseo.mvp.LoginMVP;

public class LoginInteractor implements LoginMVP.Model {

    private ProductRepository productRepository;

    private UserRepository userRepository;

    private FirebaseAuthRepository firebaseAuthRepository;


    public LoginInteractor(Context context){

       // userRepository = new UserRepository(context);
        userRepository = UserRepository.getInstance(context);
        productRepository = new ProductRepository();

        firebaseAuthRepository = FirebaseAuthRepository.getInstance(context);


    }


    @Override
    public void validateCredentials(String email, String password, ValidateCredentialsCallback callback) {

        firebaseAuthRepository.authenticate(email, password, new FirebaseAuthRepository.FirebaseAuthCallBack() {
            @Override
            public void onSuccess() {
                    callback.onSuccess();
            }

            @Override
            public void onFailure() {
                    callback.onFailure();
            }
        });

        /*
        userRepository.getUserByEmail(email, new UserRepository.UserCallback<User>() {
            @Override
            public void onSuccess(User user) {

                if (user == null) {
                    callback.onFailure("el usuario no existe");
                } else if (!user.getPassword().equals(password)) {
                    callback.onFailure("contraseña incorrecta");
                } else {
                    callback.onSuccess();
                }
            }

            @Override
            public void onFailure() {
                callback.onFailure("Error accediendo a la fuente de datos");
            }
        });

        userRepository.getAll(new UserRepository.UserCallback<List<User>>() {
            @Override
            public void onSuccess(List<User> data) {
               for (User user : data){
                   Log.d(LoginInteractor.class.getSimpleName(), user.toString());
               }
            }

            @Override
            public void onFailure() {
                Log.w(LoginInteractor.class.getSimpleName(), "problemas al obtener datos");
            }
        });

        productRepository.getByCode("61a52975e5a4a569ea358c0d", new ProductRepository.ProductCallback<ProductResponse>() {
            @Override
            public void onSuccess(ProductResponse data) {
                Log.i("product-getbycode", data.toString());
            }

            @Override
            public void onFailure(String error) {
                Log.e("product-getbycode", error);
            }
        });

        productRepository.getAll(new ProductRepository.ProductCallback<List<ProductResponse>>() {
            @Override
            public void onSuccess(List<ProductResponse> data) {
                Log.i("product-getall", data.toString());
            }

            @Override
            public void onFailure(String error) {
                Log.e("product-getall", error);
            }
        });

        ProductRequest productRequest = new ProductRequest("1223", "12133234", 16, "lic.arnulveca@gmail.com");

        productRepository.create(productRequest,
                new ProductRepository.ProductCallback<ProductResponse>() {
                    @Override
                    public void onSuccess(ProductResponse data) {
                        Log.i("product-getbycode", data.toString());
                    }

                    @Override
                    public void onFailure(String error) {
                        Log.e("product-getbycode", error);
                    }
                });

    */


    }

    @Override
    public boolean isAuthenticated() {
        return firebaseAuthRepository.isAuthenticated();
    }

    @Override
    public Intent getGoogleIntent() {
        return firebaseAuthRepository.getGoogleSingInIntent();
    }


    @Override
    public void setGoogleData(Intent data, ValidateCredentialsCallback callback) {
        firebaseAuthRepository.setGoogleData(data, new FirebaseAuthRepository.FirebaseAuthCallBack() {
            @Override
            public void onSuccess() {
                callback.onSuccess();
            }

            @Override
            public void onFailure() {
                callback.onFailure();
            }
        });
    }

}

