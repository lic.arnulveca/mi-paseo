package co.com.devprofesor.mipaseo.model.http.dto;

   /*
    {
        "_id": "61644e2f335aa93ff030d197",
            "nombre": "Jeniffer",
            "apellido": "Estrada",
            "edad": 18,
            "email": "jenian19@gmail.com",
            "__v": 0
    }

    */

import co.com.devprofesor.mipaseo.model.repository.ProductRepository;

public class ProductRequest {

    private String nombre;
    private String Apellido;
    private Integer edad;
    private String email;


    public ProductRequest() {

    }

    public ProductRequest(String nombre, String apellido, Integer edad, String email) {
        this.nombre = nombre;
        Apellido = apellido;
        this.edad = edad;
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
