package co.com.devprofesor.mipaseo.model.http;


import java.util.List;

import co.com.devprofesor.mipaseo.model.http.dto.ProductRequest;
import co.com.devprofesor.mipaseo.model.http.dto.ProductResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ProductApi {


    @GET("users")
    Call<List<ProductResponse>> getAll();

    @GET("users/{id}")
    Call<ProductResponse> getByCode(@Path("id") String _id);

    @POST("users")
    Call<ProductResponse> createProduct(@Body ProductRequest product);
}
