package co.com.devprofesor.mipaseo.model.repository;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.identity.SignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.concurrent.Executor;

import co.com.devprofesor.mipaseo.R;
import co.com.devprofesor.mipaseo.model.database.entities.User;
import co.com.devprofesor.mipaseo.mvp.LoginMVP;

public class FirebaseAuthRepository {



    public static FirebaseAuthRepository getInstance(Context context) {
        if(instance==null){
            instance = new FirebaseAuthRepository(context);
        }
        return instance;
    }

    private static FirebaseAuthRepository instance;



    private UserRepository userRepository;
    private final FirebaseAuth firebaseAuth;
    private FirebaseUser currentUser;
    private final GoogleSignInClient googleSignInClient;

    private FirebaseAuthRepository(Context context) {

        this.userRepository = UserRepository.getInstance(context);
        firebaseAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("860600310482-f44e721tloege4ncblr78bcibjd77vam.apps.googleusercontent.com")
                .requestEmail()
                .build();

        googleSignInClient = GoogleSignIn.getClient(context, gso);

    }


    public void registerNewUser(String email, String password, FirebaseAuthCallBack callBack){
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(task ->  {
                           if (task.isSuccessful()) {

                            currentUser = firebaseAuth.getCurrentUser();
                               callBack.onSuccess();
                        } else {
                           callBack.onFailure();
                        }
                });
    }

    public void authenticate(String email, String password, FirebaseAuthCallBack callBack){
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task ->  {
                        if (task.isSuccessful()) {
                            currentUser = firebaseAuth.getCurrentUser();
                            registerNewUser(email, password, callBack);
                            callBack.onSuccess();

                        } else {
                            callBack.onFailure();
                        }

                });
    }

    public void logout(FirebaseAuthCallBack callBack){
        if(currentUser != null){
            firebaseAuth.signOut();
            currentUser = null;
            callBack.onSuccess();
        }else{
            callBack.onFailure();
        }
    }


    public boolean isAuthenticated() {

        return getCurrentUser() != null;

    }


    public FirebaseUser getCurrentUser() {

        if(currentUser == null){
            currentUser = firebaseAuth.getCurrentUser();

        }
        return currentUser;
    }

    public void setGoogleData(Intent data, FirebaseAuthCallBack callback) {
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            firebaseAuthwithGoogle(account.getIdToken(), callback);
        } catch (ApiException e) {
            callback.onFailure();
        }
    }

    private void firebaseAuthwithGoogle(String idToken, FirebaseAuthCallBack callBack) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        currentUser = firebaseAuth.getCurrentUser();
                        callBack.onSuccess();
                    } else {
                        callBack.onFailure();
                    }
                });

    }



    public interface FirebaseAuthCallBack{
        void onSuccess();
        void onFailure();
    }


    public Intent getGoogleSingInIntent(){
         return googleSignInClient.getSignInIntent();
    }
}
