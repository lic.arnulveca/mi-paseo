package co.com.devprofesor.mipaseo.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Date;

import co.com.devprofesor.mipaseo.R;

public class VentaCuadro extends AppCompatActivity {

    private TextInputLayout til_client;
    private TextInputEditText et_client;

    private TextInputLayout til_adrres;
    private TextInputEditText et_adres;

    private TextInputLayout til_amount;
    private TextInputEditText et_amount;

    private TextInputLayout til_puntos;
    private TextInputEditText et_puntos;

    private TextInputLayout til_efectivo;
    private MaterialAutoCompleteTextView et_efectivo;

    private TextInputLayout til_part;
    private TextInputEditText et_part;

    private TextInputLayout til_date;
    private TextInputEditText et_date;

    private TextInputLayout til_siguientepago;
    private TextInputEditText et_siguientepago;

    private AppCompatButton btn_pagos;

    private Date SelectedDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venta_cuadro);
        initui();
    }

    private void initui(){
        til_date = findViewById(R.id.til_date);
        til_date.setEndIconOnClickListener(v -> onDateClick());

        et_date = findViewById(R.id.et_date);
    }

    private void onDateClick(){

        if (SelectedDate == null){
            SelectedDate = new Date();

        }


        MaterialDatePicker<Long> datePicker = MaterialDatePicker.Builder
                .datePicker()
                .setSelection(SelectedDate.getTime())
                .setTitleText("Seleccionar fecha")
                .build();

        datePicker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener<Long>() {
            @Override
            public void onPositiveButtonClick(Long selection) {
                onDateSelection(selection);
            }
        });

        datePicker.show(getSupportFragmentManager(), "date");
    }

    private void onDateSelection(long selection){
        Toast.makeText(VentaCuadro.this, "" + selection, Toast.LENGTH_SHORT).show();

        Date fecha = new Date(selection);
        et_date.setText(SimpleDateFormat.getDateInstance().format(fecha));
    }

}