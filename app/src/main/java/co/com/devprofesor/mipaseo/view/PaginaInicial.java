package co.com.devprofesor.mipaseo.view;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.firebase.auth.FirebaseUser;

import co.com.devprofesor.mipaseo.R;
import co.com.devprofesor.mipaseo.model.repository.FirebaseAuthRepository;


public class PaginaInicial extends AppCompatActivity {


    private Button btn_home;
    private Button btn_user;
    private Button btn_sale;
    private Button btn_points;
    public TextView usertitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagina_inicial);


        initPanta();
        loadData();

    }

    private void initPanta() {
        btn_home = findViewById(R.id.btn_home);
        btn_user = findViewById(R.id.btn_user);
        btn_sale = findViewById(R.id.btn_sale);
        btn_points = findViewById(R.id.btn_points);
        usertitle = findViewById(R.id.usertitle);

        btn_home.setOnClickListener((evt) -> { onHomeclick(); });
        btn_user.setOnClickListener((evt) -> { onUserclick(); });
        btn_sale.setOnClickListener((evt) -> { onSaleclick(); });
        btn_points.setOnClickListener((evt) -> { onPointsclick(); });
        
    }

    private void onPointsclick() {
        Toast.makeText(this, "intentas cerrar sesion", Toast.LENGTH_SHORT).show();

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);


            builder.setTitle(R.string.app_name)
                    .setMessage("Esta seguro de cerrar sesion")
                    .setPositiveButton("OK", (dialog, which) -> {
                        FirebaseAuthRepository.getInstance(PaginaInicial.this).logout(new FirebaseAuthRepository.FirebaseAuthCallBack() {
                            @Override
                            public void onSuccess() {

                                Intent intent = new Intent(PaginaInicial.this, LoginActivity.class);
                                startActivity(intent);

                            }

                            @Override
                            public void onFailure() {
                                Toast.makeText(PaginaInicial.this, "error al cerrar sesion", Toast.LENGTH_SHORT).show();
                            }
                        });

                    })
                    .setNegativeButton("CANCELAR", null);

            builder.create().show();
        }



    private void onSaleclick() {
        Toast.makeText(this, "vamos a compras y ventas", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, cambiarPuntosxProductos.class);
        startActivity(intent);
    }

    private void onUserclick() {
        Toast.makeText(this, "perfil de la persona abierto", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, PerfilPersona.class);
        startActivity(intent);
    }

    private void onHomeclick() {
        Toast.makeText(this, "pagina de home", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, PaginaInicial.class);
        startActivity(intent);
    }

    private void loadData(){


        FirebaseAuthRepository repository = FirebaseAuthRepository.getInstance(this);
        FirebaseUser user = repository.getCurrentUser();
        usertitle.setText(user.getDisplayName());
    }


}